package co.arpsoft.runner;


import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/testimTest.feature"},
        glue = {"co.arpsoft.stepsdefinition"},
        tags = {""}
)
public class TestimRunner {
}
