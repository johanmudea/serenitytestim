package co.arpsoft.stepsdefinition;

import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;

public class Setup {

    @Managed()
    protected WebDriver webDriver;

    protected void setupUser(String name, WebDriver webDriver ){
        OnStage.setTheStage(new OnlineCast()); // Se configura el stage(ambiente) donde el actor va a realzuar las acciones.
        theActorCalled(name).can(BrowseTheWeb.with(webDriver));
    }

    protected void userSetupBrowser(String name){
        setupUser(name,webDriver); // si utlizo esta configuracion solo se le debe pasar el nimbre, ya que el driver viene desde este metodo
    }



}
