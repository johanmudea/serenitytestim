package co.arpsoft.stepsdefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hamcrest.Matchers;

import static co.arpsoft.question.LoginSuccesfulAssertQuestion.loginSuccesfulAssertQuestion;
import static co.arpsoft.task.BrowseToLoginTask.browseToLoginTask;
import static co.arpsoft.task.FillLoginCredentialsTask.fillLoginCredentialsTask;
import static co.arpsoft.task.OpenLandinPageTask.openLandinPage;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class TestimSteps extends Setup {

    private static final  String ACTOR_NAME = "JohanM";
    private static final Logger logger = LogManager.getLogger(TestimSteps.class);



    @Given("Navego al inicio de testim")
    public void navegoAlInicioDeTestim() {
        try {
            userSetupBrowser(ACTOR_NAME);
            theActorInTheSpotlight().wasAbleTo(
                    openLandinPage()
            );

        } catch (Exception e){

            logger.error("error @given: Navego al inicio de testim");
        }
    }


    @When("me dirijo al login")
    public void meDirijoAlLogin() {
        try {
            theActorInTheSpotlight().attemptsTo(
                    browseToLoginTask()
            );
        }catch (Exception e){
            logger.error("error @When(me dirijo al login");

        }

    }

    @When("ingreso user y tambien Password")
    public void ingresoUserYTambienPassword() {
        try {
            theActorInTheSpotlight().attemptsTo(
                    fillLoginCredentialsTask()
            );

        }catch (Exception e){
            logger.error("error @When(ingreso user y tambien Password");

        }


    }

    @Then("deberia ver saludo de bienvenida")
    public void deberiaVerSaludoDeBienvenida() {
        try {
            theActorInTheSpotlight().should(
                    seeThat(loginSuccesfulAssertQuestion(), Matchers.equalTo(true))
            );

        }catch (Exception e){
            logger.error("error @Then(deberia ver saludo de bienvenida", e);

        }


    }
}
