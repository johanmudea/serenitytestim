package co.arpsoft.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.arpsoft.userinterface.HomePageUI.LOGIN;

public class BrowseToLoginTask implements Task {

    @Override
    public <T extends Actor> void performAs(T t) {
        t.attemptsTo(
                Scroll.to(LOGIN),
                Click.on(LOGIN)

        );

    }


    public static BrowseToLoginTask browseToLoginTask(){
        return new BrowseToLoginTask();
    }
}
