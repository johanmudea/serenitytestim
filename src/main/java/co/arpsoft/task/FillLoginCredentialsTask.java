package co.arpsoft.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.type.TypeValue;

import static co.arpsoft.userinterface.LoginPageUI.*;

public class FillLoginCredentialsTask implements Task {

    @Override
    public <T extends Actor> void performAs(T t) {

        t.attemptsTo(

                Scroll.to(LOGIN_USERNAME),
                Enter.theValue("Johan").into(LOGIN_USERNAME),

                Scroll.to(LOGIN_PASSWORD),
                Enter.theValue("Muñetón").into(LOGIN_PASSWORD),

                Scroll.to(LOGIN_USERNAME),
                Click.on(LOGIN_BUTTON)

        );

    }

    public static FillLoginCredentialsTask fillLoginCredentialsTask(){
        return new FillLoginCredentialsTask();
    }
}
