package co.arpsoft.task;

import co.arpsoft.userinterface.LandingPageUI;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

public class OpenLandinPageTask implements Task {

    LandingPageUI testimLandingPage;
    @Override
    public <T extends Actor> void performAs(T t) {
        t.attemptsTo(
                Open.browserOn(testimLandingPage)
        );

    }



    public static OpenLandinPageTask openLandinPage(){
        return new OpenLandinPageTask();
    }

}
