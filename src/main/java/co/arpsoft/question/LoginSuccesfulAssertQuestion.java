package co.arpsoft.question;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.arpsoft.userinterface.HomePageUI.TEXT_LOGGED_BUTTON;

public class LoginSuccesfulAssertQuestion implements Question<Boolean> {


    @Override
    public Boolean answeredBy(Actor actor) {
        return (TEXT_LOGGED_BUTTON.resolveFor(actor).containsOnlyText("HELLO, JOHN"));
    }



    public static LoginSuccesfulAssertQuestion loginSuccesfulAssertQuestion(){
        return new LoginSuccesfulAssertQuestion();
    }
}
