package co.arpsoft.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

import static org.openqa.selenium.By.xpath;

public class LoginPageUI extends PageObject {

    public static final Target LOGIN_USERNAME = Target.the("LOGIN_USERNAME").located(xpath("//input[contains(@tabindex,'1') and contains(@type,'text') ]"));
    public static final Target LOGIN_PASSWORD = Target.the("LOGIN_PASSWORD").located(xpath("//input[contains(@tabindex,'2') and contains(@type,'password') ]"));
    public static final Target LOGIN_BUTTON = Target.the("LOGIN_BUTTON").located(xpath("//button[contains(@tabindex,'3') and contains(@type,'submit') ]"));



}
