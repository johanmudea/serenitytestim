package co.arpsoft.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

import static org.openqa.selenium.By.xpath;

public class HomePageUI extends PageObject {

    public static final Target LOGIN = Target.the("Login Button").located(xpath("//*[@id=\"app\"]/div/header/div/div[2]/ul/li/button"));

    public static final Target TEXT_LOGGED_BUTTON = Target.the("TEXT_LOGGED_BUTTON").located(xpath("//*[@id=\"app\"]/div/header/div/div[2]/ul/div/button/span[1]"));


}
